# Gamerpro API

The Gamerpro API enables you to communicate with Gamerpro backend using a simple REST API.

You can find the [OpenAPI specification](#openapi-specification) on our Gitlab repository. This specification contains the full documentation of the API.

The Gamerpro frontend and the Game frontend are communicates each other with [deep links](#deep-linking).

## Prerequisites

Gamerpro needs the following informations about the Game:

- Package name (Android) and/or Bundle ID (iOS)
- Deep link base URL

Gamerpro gives the following informations:

- API key
- API secret

### Deep linking

The Gamerpro frontend needs to communicate with the Game frontend. Because of this it needs to implement deep link handling on the Game side. There are two types of deep links:

- simple deep link
- deferred deep link

The main difference is that deferred deep links support users to install the Game if they have not installed it yet. Hence, it is highly recommended to implement the deferred deep link to ensure a smooth user acquisition journey.

We recommend using [Android App Links](#android-app-links) on Andriod and [iOS Universal Links](#ios-universal-links) on iOS.

## Flows that need to implement

The following flow diagrams contains the flows that need to implement on the Game side.

The calling of the `gamerpro.v1.Gamer/Token` endpoint has to be implemented on the Game backend server, behind the Game’s authorization mechanism. The rest of the endpoints can be called from the frontend, but calling from the backend is highly recommended for security reasons.

The `gamerpro.v1.Gamer/Token` endpoint generated the gamer token. This endpoint is using http basic authentication. The username is the API key and the password is the API secret. The gamer token is in the `token` field of the response JSON.

### Open Gamerpro

When the gamer tap on the esports button in the Game we need to write the current gamer data to the Gamerpro server and gets the deep link for open Gamerpro app.

We can do that with two steps:

1. Call the `gamerpro.v1.Gamer/Token` endpoint for a gamer token.
2. After we have a gamer token we can call the `gamerpro.v1.Gamer/Write` endpoint. This endpoint is writing the gamer data to the Gamerpro server. This endpoint is using http bearer token based authentication. We need to add an `Authorization` header to the request with this value: `Bearer <gamer token>`. The response is a `GamerData` object. This object has a `deepLink` field. We need to open this URL in external browser. If the Gamerpro app is installed this link opens the app. If it not installed, then starting the installation flow because this deep link is a deferred deep link.

```mermaid
sequenceDiagram
participant Game as Game
participant Backend as Gamerpro Backend
participant Frontend as Gamerpro Frontend

Note over Game: When a gamer tap on the esports button
Game->>+Backend: gamerpro.v1.Gamer/Token
Backend-->>-Game: JWT token
Game->>+Backend: gamerpro.v1.Gamer/Write
Backend-->>-Game: GamerData
Game->>Frontend: Open Gamerpro with GamerData.deepLink
```

### Update gamer data

When the gamer data changes we need to write the current state of the data to the Gamerpro server.

We can do that with two steps:

1. Call the `gamerpro.v1.Gamer/Token` endpoint for a gamer token.
2. After we have a gamer token we can call the `gamerpro.v1.Gamer/Write` endpoint. This endpoint is writing the gamer data to the Gamerpro server. This endpoint is using http bearer token based authentication. We need to add an `Authorization` header to the request with this value: `Bearer <gamer token>`. The response is a `GamerData` object with the actual state of the gamer data on the Gamerpro server.

```mermaid
sequenceDiagram
participant Game as Game
participant Backend as Gamerpro Backend

Note over Game: If some of the required gamer properties are changed<br>(display name, profile image url, level)
Game->>+Backend: gamerpro.v1.Gamer/Token
Backend-->>-Game: JWT token
Game->>+Backend: gamerpro.v1.Gamer/Write
Backend-->>-Game: GamerData
```

### Playing the match

When the user wants to play a measured match with the Game, we need to open the Game with a deep link with `matchId` parameter. The Game should create a new game based on the match given by Gamerpro. We can gets the match data from the API. After the game is created by the match data and the gamer started the game we need to send a start signal to the Gamerpro server. When the game is over we need to send the result to the Gamerpro server.

We can do the flow above with the following steps:

1. Call the `gamerpro.v1.Gamer/Token` endpoint for a gamer token.
2. Call the `gamerpro.v1.Match/Read` endpoint for the match data. This endpoint is using http bearer token based authentication. We need to add an `Authorization` header to the request with this value: `Bearer <gamer token>`. The response is a `MatchData` object with the actual state of the match on the Gamerpro server. We can create the game based on this data.
3. Once the gamer starts the game we need to call the `gamerpro.v1.Match/Start` endpoint for notify Gamerpro. This endpoint is using http bearer token based authentication. We need to add an `Authorization` header to the request with this value: `Bearer <gamer token>`. The response is a `MatchData` object with the actual state of the match on the Gamerpro server.
4. After the game is over we need to call the `gamerpro.v1.Match/SetResult` endpoint for send the result to Gamerpro. This endpoint is using http bearer token based authentication. We need to add an `Authorization` header to the request with this value: `Bearer <gamer token>`. The response is a `MatchData` object with the actual state of the match on the Gamerpro server.

```mermaid
sequenceDiagram
participant Frontend as Gamerpro Frontend
participant Game as Game
participant Backend as Gamerpro Backend

Note over Frontend: When a user want to play a match on Gamerpro
Frontend->>Game: Open Game with deep link
Note over Game: Creating new game
Game->>+Backend: gamerpro.v1.Gamer/Token
Backend-->>-Game: JWT token
Game->>+Backend: gamerpro.v1.Match/Read
Backend-->>-Game: MatchData
Note over Game: Start the game
Game->>+Backend: gamerpro.v1.Gamer/Token
Backend-->>-Game: JWT token
Game->>+Backend: gamerpro.v1.Match/Start
Backend-->>Game: MatchData
Backend-->>-Frontend: Match started
Note over Game: Finish the game
Game->>+Backend: gamerpro.v1.Gamer/Token
Backend-->>-Game: JWT token
Game->>+Backend: gamerpro.v1.Match/SetResult
Backend-->>Game: MatchData
Backend-->>-Frontend: Match result
```

## Support

If you have a questions, or want to report an issue for us please login to Gitlab and [create an issue](https://gitlab.com/gamerpro/pub/api/-/issues/new) yourself or [send an email](mailto:contact-project+gamerpro-pub-api-issue@incoming.gitlab.com) which is created it automatically.

## Dictionnary

### gamer

user on the Game side

### user

user on Gamerpro side

### game

the match in the Game

### match

the match in Gamerpro

## References

### [OpenAPI specification](openapi_gamerpro.yaml)

### [Bearer Authentication](https://swagger.io/docs/specification/authentication/bearer-authentication/)

### [JSON Web Token](https://jwt.io)

### [Custom URL scheme on Android](https://developer.android.com/training/app-links/deep-linking)

### [Custom URL scheme on iOS](https://developer.apple.com/documentation/xcode/allowing_apps_and_websites_to_link_to_your_content/defining_a_custom_url_scheme_for_your_app)

### [Android App Links](https://developer.android.com/training/app-links)

### [iOS Universal Links](https://developer.apple.com/ios/universal-links)

### [IOS Deep linking: URL Scheme vs Universal Links](https://medium.com/wolox/ios-deep-linking-url-scheme-vs-universal-links-50abd3802f97)
